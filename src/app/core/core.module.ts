import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AsyncPipe, CurrencyPipe, DecimalPipe } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { ToastrModule } from "ngx-toastr";
import { NgModule } from "@angular/core";

import { SharedModule } from "../shared/shared.module";
import { PagesModule } from "../pages/pages.module";
import { HeaderModule } from "./components/header/header.module";

@NgModule({
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        HttpClientModule,
        SharedModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            closeButton: true
        }),
    ],
    exports: [
        BrowserAnimationsModule,
        BrowserModule,
        HttpClientModule,
        PagesModule,
        ToastrModule,
        HeaderModule,
    ],
    providers: [
        CurrencyPipe,
        DecimalPipe,
        AsyncPipe,
    ],
})
export class CoreModule {
}
