import { BehaviorSubject, Observable } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root",
})
export class ChannelsStateService {

    public selectedChannel$: Observable<number>;

    private selectedChannelSource: BehaviorSubject<number>

    constructor() {
        this.selectedChannelSource = new BehaviorSubject<number>(null);
        this.selectedChannel$ = this.selectedChannelSource.asObservable();
    }

    public selectChannel(id?: number)
    : void {
        this.selectedChannelSource.next(id);
    }

    public unselectChannel()
    : void {
        this.selectedChannelSource.next(null);
    }
}
