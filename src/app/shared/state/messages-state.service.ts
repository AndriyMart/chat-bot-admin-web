import { BehaviorSubject, Observable } from "rxjs";
import { Injectable } from "@angular/core";
import * as _ from "lodash";

@Injectable({
    providedIn: "root",
})
export class MessagesStateService {

    public selectedMessages$: Observable<number[]>;

    private selectedMessagesSource: BehaviorSubject<number[]>

    constructor() {
        this.selectedMessagesSource = new BehaviorSubject<number[]>([]);
        this.selectedMessages$ = this.selectedMessagesSource.asObservable();
    }

    public selectExactMessages(ids: number[])
    : void {
        this.selectedMessagesSource.next(ids);
    }

    public selectMessage(id: number)
    : void {
        if (!id && id !== 0) {
            return;
        }
        const values = this.selectedMessagesSource.getValue();

        if (values.includes(id)) {
            return;
        }

        values.push(id);

        this.selectedMessagesSource.next(values);
    }

    public unselectMessage(id: number)
    : void {
        if (!id && id !== 0) {
            return;
        }

        const values = this.selectedMessagesSource.getValue();
        this.selectedMessagesSource.next(_.without(values, id));
    }

    public unselectAll()
    : void {
        this.selectedMessagesSource.next([]);
    }
}
