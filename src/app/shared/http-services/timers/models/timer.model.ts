import { NamedEntity } from "../../../models/named-entity.model";

export class Timer {

    public id: number;
    public frequency: number;
    public messages: NamedEntity[] | number[];
    public channel_id: number;

    constructor(init?: Partial<Timer>) {
        Object.assign(this, init);
    }
}
