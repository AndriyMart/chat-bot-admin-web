import { AbstractHttpService } from "../abstract-http.service";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, delay } from "rxjs/operators";
import { Observable, of } from "rxjs";

import { Timer } from "./models/timer.model";

@Injectable({
    providedIn: "root",
})
export class TimersHttpService extends AbstractHttpService {

    private path: string;

    constructor(
        private httpClient: HttpClient,
    ) {
        super();

        this.path = `${this.apiUrl}/timers`;
    }

    public getAll()
    : Observable<Timer[]> {
        return this.httpClient
            .get(`${this.path}`)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );

        // return of([
        //     {
        //         id: 0,
        //         frequency: 2,
        //         messages: [0, 1],
        //         channel: "PV",
        //     },
        //     {
        //         id: 1,
        //         frequency: 1,
        //         messages: [1],
        //         channel: "Dotaaa",
        //     },
        // ])
        //     .pipe(delay(1500));
    }

    public create(timer: Partial<Timer>)
    : Observable<any> {
        return this.httpClient
            .post(`${this.path}`, timer)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
        // return of(null)
        //     .pipe(delay(500));
    }

    public delete(id: number)
    : Observable<any> {
        return this.httpClient
            .delete(`${this.path}/${id}`)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
        // return of(null)
        //     .pipe(delay(500));
    }
}
