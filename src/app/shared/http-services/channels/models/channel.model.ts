export class Channel {

    public id: number;
    public name: string;
    public streamTitle: string;
    public link: string;
    public live: boolean;

    constructor(init?: Partial<Channel>) {
        Object.assign(this, init);
    }
}
