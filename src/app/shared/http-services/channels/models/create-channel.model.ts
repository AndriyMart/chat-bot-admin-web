export class CreateChannel {

    public link: string;

    constructor(init?: Partial<CreateChannel>) {
        Object.assign(this, init);
    }
}
