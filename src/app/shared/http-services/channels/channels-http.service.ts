import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError } from "rxjs/operators";
import { Observable } from "rxjs";

import { AbstractHttpService } from "../abstract-http.service";
import { Channel } from "./models/channel.model";
import { CreateChannel } from "./models/create-channel.model";

@Injectable({
    providedIn: "root",
})
export class ChannelsHttpService extends AbstractHttpService {

    private path: string;

    constructor(
        private httpClient: HttpClient,
    ) {
        super();

        this.path = `${ this.apiUrl }/channels/`;
    }

    public getAll()
    : Observable<Channel[]> {
        return this.httpClient
            .get(`${this.path}`)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
    }

    public create(model: CreateChannel)
    : Observable<any> {
        return this.httpClient
            .post(`${this.path}`, model)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
    }

    public remove(id: number)
    : Observable<any> {
        return this.httpClient
            .delete(`${this.path}${id}`)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
    }
}

