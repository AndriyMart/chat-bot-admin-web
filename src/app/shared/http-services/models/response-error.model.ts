export class ResponseError {

    public status: string;
    public message: string;

    constructor(init?: Partial<ResponseError>) {
        Object.assign(this, init);
    }
}
