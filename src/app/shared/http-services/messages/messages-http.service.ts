import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, delay } from "rxjs/operators";
import { Observable, of } from "rxjs";

import { AbstractHttpService } from "../abstract-http.service";
import { Message } from "./models/message.model";
import { NamedEntity } from "../../models/named-entity.model";

@Injectable({
    providedIn: "root",
})
export class MessagesHttpService extends AbstractHttpService {

    private path: string;
    private groupsPath: string;

    constructor(
        private httpClient: HttpClient,
    ) {
        super();

        this.path = `${this.apiUrl}/messages/`;
    }

    public getAll()
    : Observable<Message[]> {
        return this.httpClient
            .get(`${this.path}`)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
    }

    public createMessage(message: Message)
    : Observable<any> {
        return this.httpClient
            .post(`${this.path}`, message)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
    }

    public updateMessage(message: Message)
    : Observable<any> {
        return this.httpClient
            .patch(`${this.path}${message.id}`, message)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
    }

    public getAllGroups()
    : Observable<NamedEntity[]> {
        return this.httpClient
            .get(`${this.path}groups`)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
        // return of([
        //     {
        //         id: 0,
        //         name: "Dota2 – Эпик Лига – RuHub",
        //     },
        //     {
        //         id: 1,
        //         name: "Valorant – WePlay! Invitational – WePlay",
        //     },
        // ])
        //     .pipe(delay(1500));
    }

    public addGroup(group: NamedEntity)
    : Observable<any> {
        return this.httpClient
            .post(`${this.path}groups`, group)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
        // return of(null)
        //     .pipe(delay(500));
    }

    public deleteGroup(id: number)
    : Observable<any> {
        return this.httpClient
            .delete(`${this.path}groups/${id}`)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
        // return of(null)
        //     .pipe(delay(500));
    }

    public updateGroup(group: NamedEntity)
    : Observable<any> {
        return this.httpClient
            .patch(`${this.path}groups/${group.id}`, group)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
        // return of(null)
        //     .pipe(delay(500));
    }

    public delete(id: number)
    : Observable<any> {
        return this.httpClient
            .delete(`${this.path}${id}`)
            .pipe(
                catchError<any, any>(this.handleError.bind(this))
            );
        // return of(null)
        //     .pipe(delay(500));
    }
}

