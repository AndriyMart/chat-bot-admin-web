export class Message {

    public id: number;
    public text: string;
    public group: string;
    public group_id?: number;

    constructor(init?: Partial<Message>) {
        Object.assign(this, init);
    }
}
