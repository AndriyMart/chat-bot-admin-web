import { HttpErrorResponse, HttpHeaders, HttpParams } from "@angular/common/http";
import { ErrorObservable } from "rxjs-compat/observable/ErrorObservable";
import { throwError as observableThrowError } from "rxjs";

import { environment } from "../../../environments/environment";
import { ResponseError } from "./models/response-error.model";
import { WrappedError } from "./models/wrapped-error.model";

export class AbstractHttpService {

    private _apiUrl: string;

    constructor() {
        this._apiUrl = environment.apiUrl;
    }

    public get apiUrl()
    : string {
        return `${this._apiUrl}/api`;
    }

    public get acceptJsonHeaders()
    : HttpHeaders {
        return new HttpHeaders().set("Accept", "application/json");
    }

    public handleError(response: HttpErrorResponse)
    : ErrorObservable<HttpErrorResponse> {
        return observableThrowError(
            this.wrapError(response)
        );
    }

    public buildHttpParams(obj: { [key: string]: any } | null)
    : HttpParams {
        let httpParams = new HttpParams();

        if (obj !== null) {
            Object
                .keys(obj)
                .filter((key) => !this.isEmpty(obj[key]))
                .forEach((key) => httpParams = httpParams.set(key, obj[key]));
        }

        return httpParams;
    }

    public buildFlatHttpParams(obj: { [key: string]: any })
    : HttpParams {
        const flattedObject = this.flatObject(obj);
        return this.buildHttpParams(flattedObject);
    }

    private flatObject(obj: { [key: string]: any })
    : { [key: string]: any } {
        const flattedObject = {};

        this.addToFlatObject(obj, flattedObject);

        return flattedObject;
    }

    private addToFlatObject(
        subObj: { [key: string]: any },
        flattedObj: { [key: string]: any },
        parentKey?: string
    )
    : void {
        if (!subObj) {
            return ;
        }

        Object.keys(subObj)
            .forEach((key) => {
                const newKey = parentKey ?
                    parentKey + "." + key
                    : key;

                if (typeof subObj[key] === "object") {
                    this.addToFlatObject(subObj[key], flattedObj, newKey);
                } else {
                    flattedObj[newKey] = subObj[key];
                }
            });
    }

    public serializeJsonPayload(object: Object)
    : string {
        return normalize(JSON.stringify(object));

        function normalize(str: string)
            : string {
            const doubleQuote = "\"";
            const firstChar = str.charAt(0);
            const lastChar = str.charAt(str.length - 1);

            if (firstChar === doubleQuote && lastChar === doubleQuote) {
                return str.substr(1, str.length - 2);
            } else {
                return str;
            }
        }
    }

    public wrapError(response: HttpErrorResponse)
    : WrappedError {
        const responseError: ResponseError = (response && response.error)
            ? response.error
            : null;

        return new WrappedError({ response, responseError });
    }

    private isEmpty(value: any)
    : boolean {
        return  value === null  || typeof value === "undefined";
    }
}
