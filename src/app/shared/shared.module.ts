import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { BrowserModule } from "@angular/platform-browser";
import { NgxSmartModalModule } from "ngx-smart-modal";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { SegmentLoaderModule } from "./page-loader/segment-loader.module";
import { PipesModule } from "./pipes/pipes.module";

@NgModule({
    imports: [
        // Angular modules
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        //Vendor modules
        NgxSmartModalModule.forRoot(),
    ],
    declarations: [
    ],
    exports: [
        // Angular modules
        BrowserAnimationsModule,
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        MatCheckboxModule,

        // Vendor Modules
        NgxSmartModalModule,

        // Internal Modules
        SegmentLoaderModule,
        PipesModule,
    ],
})
export class SharedModule { }
