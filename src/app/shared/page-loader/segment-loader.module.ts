import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { NgModule } from "@angular/core";

import { SegmentLoaderComponent } from "./components/segment-loader.component";

@NgModule({
    imports: [
        MatProgressSpinnerModule,
    ],
    declarations: [
        SegmentLoaderComponent,
    ],
    exports: [
        SegmentLoaderComponent,
    ]
})
export class SegmentLoaderModule {
}
