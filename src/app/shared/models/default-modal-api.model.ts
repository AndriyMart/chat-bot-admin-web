export class EditorModalApi<T> {

    public openModal: (subject?: T) => void;

    constructor(init?: Partial<EditorModalApi<T>>) {
        Object.assign(this, init);
    }
}
