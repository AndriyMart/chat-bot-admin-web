export class NamedEntity {

    public id: number;
    public name: string;

    constructor(init?: Partial<NamedEntity>) {
        Object.assign(this, init);
    }
}
