import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MailingsComponent } from "./pages/mailings/components/mailings.component";

const routes: Routes = [{
    path: "",
    component: MailingsComponent,
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
