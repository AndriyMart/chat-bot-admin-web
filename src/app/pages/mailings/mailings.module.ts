import { NgModule } from "@angular/core";

import { ChannelsBarModule } from "../../components/channels-bar/channels-bar.module";
import { MessagesBarModule } from "../../components/messages-bar/messages-bar.module";
import { MailingsComponent } from "./components/mailings.component";
import { TimersBarModule } from "../../components/timers-bar/timers-bar.module";

@NgModule({
    imports: [
        ChannelsBarModule,
        MessagesBarModule,
        TimersBarModule,
    ],
    declarations: [
        MailingsComponent,
    ],
    exports: [
        MailingsComponent,
    ],
})
export class MailingsModule { }
