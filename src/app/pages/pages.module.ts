import { NgModule } from "@angular/core";

import { MailingsModule } from "./mailings/mailings.module";

@NgModule({
    exports: [
        MailingsModule,
    ],
})
export class PagesModule { }
