import { NgModule } from "@angular/core";

import { ChannelsBarComponent } from "./components/channels-bar.component";
import { ChannelComponent } from "./components/channel/channel.component";
import { SharedModule } from "../../shared/shared.module";
import { ChannelEditorComponent } from "./components/channel-editor/channel-editor.component";

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        ChannelComponent,
        ChannelsBarComponent,
        ChannelEditorComponent,
    ],
    exports: [
        ChannelsBarComponent,
    ],
})
export class ChannelsBarModule { }
