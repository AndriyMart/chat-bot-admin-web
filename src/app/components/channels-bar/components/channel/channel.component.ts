import { Component, EventEmitter, Input, Output } from "@angular/core";

import { Channel } from "../../../../shared/http-services/channels/models/channel.model";

@Component({
    selector: "clmr-channel",
    templateUrl: "./channel.component.html",
    styleUrls: ["./channel.component.scss"]
})
export class ChannelComponent {

    @Input()
    public channel: Channel;
    @Input()
    public selected: boolean;

    @Output()
    public delete: EventEmitter<void>;

    constructor() {
        this.delete = new EventEmitter<void>();
    }

    public onDelete(event: Event)
    : void {
        const confirmMessage = `Are you sure you want to delete Channel ${this.channel.name}?`

        if (!confirm(confirmMessage)) {
            return;
        }

        event.stopPropagation();

        this.delete.emit();
    }
}
