import { untilDestroyed } from "ngx-take-until-destroy";
import { Component, OnDestroy } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { finalize } from "rxjs/operators";

import { ChannelsHttpService } from "../../../shared/http-services/channels/channels-http.service";
import { Channel } from "../../../shared/http-services/channels/models/channel.model";
import { ChannelsStateService } from "../../../shared/state/channels-state.service";
import { EditorModalApi } from "../../../shared/models/default-modal-api.model";

@Component({
    selector: "clmr-channels-bar",
    templateUrl: "./channels-bar.component.html",
    styleUrls: ["./channels-bar.component.scss"]
})
export class ChannelsBarComponent implements OnDestroy {

    public channels: Channel[];
    public loading: boolean;
    public selectedChannel: number;

    private editorApi: EditorModalApi<void>;

    constructor(
        private channelsStateService: ChannelsStateService,
        private channelsHttpService: ChannelsHttpService,
        private toastrService: ToastrService,
    ) {
        this.loadData();
        this.subscribeOnSelectedChannel();
    }

    public ngOnDestroy() { }

    public onAddNew()
    : void {
        this.editorApi.openModal();
    }

    public onClick(channel: Channel)
    : void {
        channel.id === this.selectedChannel
            ? this.channelsStateService.unselectChannel()
            : this.channelsStateService.selectChannel(channel.id);
    }

    public onDelete(channel: Channel)
    : void {
        this.channelsHttpService.remove(channel.id)
            .pipe(finalize(() => this.onReload()))
            .subscribe(
                () => this.toastrService.success(`Channel ${channel.name} was successfully deleted.`),
                err => this.toastrService.error(err.message),
            )
    }

    public onEditorReady(api: EditorModalApi<void>)
    : void {
        this.editorApi = api;
    }

    public onReload()
    : void {
        this.loadData();
    }

    private loadData()
    : void {
        this.loading = true;

        this.channelsHttpService.getAll()
            .pipe(
                finalize(() => this.loading = false)
            )
            .subscribe(
                res => this.channels = res,
                err => this.toastrService.error(err.message),
            );
    }

    private subscribeOnSelectedChannel()
    : void {
        this.channelsStateService.selectedChannel$
            .pipe(untilDestroyed(this))
            .subscribe(id => this.selectedChannel = id);
    }
}
