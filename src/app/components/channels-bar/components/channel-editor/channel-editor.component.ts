import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgxSmartModalComponent } from "ngx-smart-modal";
import { ToastrService } from "ngx-toastr";

import { AbstractFormBasedComponent } from "../../../../core/components/abstract-form-based.component";
import { ChannelsHttpService } from "../../../../shared/http-services/channels/channels-http.service";
import { EditorModalApi } from "../../../../shared/models/default-modal-api.model";

@Component({
    selector: "clmr-channel-editor",
    templateUrl: "./channel-editor.component.html",
    styleUrls: [ "./channel-editor.component.scss" ]
})
export class ChannelEditorComponent extends AbstractFormBasedComponent implements OnInit {

    @ViewChild("modal")
    public modalComponent: NgxSmartModalComponent;

    @Output()
    public saved: EventEmitter<void>;
    @Output()
    public modalReady: EventEmitter<EditorModalApi<void>>;

    public form: FormGroup;

    constructor(
        private channelsHttpService: ChannelsHttpService,
        private formBuilder: FormBuilder,
        private toastrService: ToastrService,
    ) {
        super();

        this.saved = new EventEmitter<void>();
        this.modalReady = new EventEmitter<EditorModalApi<void>>();
    }

    public ngOnInit()
    : void {
        this.modalReady.emit({
            openModal: this.openModal.bind(this),
        });
    }

    public onSave()
    : void {
        this.markAllFormControlsTouched(this.form);

        if (!this.form.valid) {
            return ;
        }

        this.channelsHttpService.create({link: this.form.value.link})
            .subscribe(
                res => {
                    this.toastrService.success("Channel was successfully saved");
                    this.saved.emit();
                    this.modalComponent.close();
                },
                err => this.toastrService.error(err.message),
            )
    }

    private openModal()
    : void {
        this.initForm();
        this.modalComponent.open();
    }

    private initForm()
    : void {
        this.form = this.formBuilder.group({
            link: [null, Validators.required],
        })
    }
}
