import { NamedEntity } from "../../../../../shared/models/named-entity.model";

export class MessageGroupEditorApi {

    public openModal: (group?: NamedEntity) => void;

    constructor(init?: Partial<MessageGroupEditorApi>) {
        Object.assign(this, init);
    }
}
