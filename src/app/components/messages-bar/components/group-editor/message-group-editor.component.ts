import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgxSmartModalComponent } from "ngx-smart-modal";
import { ToastrService } from "ngx-toastr";

import { AbstractFormBasedComponent } from "../../../../core/components/abstract-form-based.component";
import { NamedEntity } from "../../../../shared/models/named-entity.model";
import { MessageGroupEditorApi } from "./models/message-group-editor-api.model";
import { MessagesHttpService } from "../../../../shared/http-services/messages/messages-http.service";

@Component({
    selector: "clmr-message-group-editor",
    templateUrl: "./message-group-editor.component.html",
    styleUrls: [ "./message-group-editor.component.scss" ]
})
export class MessageGroupEditorComponent extends AbstractFormBasedComponent implements OnInit {

    @ViewChild("modal")
    public modalComponent: NgxSmartModalComponent;

    @Output()
    public groupSaved: EventEmitter<void>;
    @Output()
    public modalReady: EventEmitter<MessageGroupEditorApi>;

    public editMode: boolean;
    public form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private messagesHttpService: MessagesHttpService,
        private toastrService: ToastrService,
    ) {
        super();

        this.groupSaved = new EventEmitter<void>();
        this.modalReady = new EventEmitter<MessageGroupEditorApi>();
    }

    public ngOnInit()
    : void {
        this.modalReady.emit({
            openModal: this.openModal.bind(this),
        });
    }

    public onSave()
    : void {
        this.markAllFormControlsTouched(this.form);

        if (!this.form.valid) {
            return ;
        }

        const group = new NamedEntity({
            id: this.form.value.id,
            name: this.form.value.name,
        });

        const source = this.editMode ? this.messagesHttpService.updateGroup : this.messagesHttpService.addGroup;

        source.bind(this.messagesHttpService)(group)
            .subscribe(
                () => {
                    this.toastrService.success(`Group ${group.name} successfully saved.`);
                    this.groupSaved.emit();
                    this.modalComponent.close();
                },
                    err => this.toastrService.error(err.message)
            );
    }

    private openModal(group?: NamedEntity)
    : void {
        this.modalComponent.open(true);
        this.editMode = !!group;

        this.initForm(group);
    }

    private initForm(group = new NamedEntity())
    : void {
        this.form = this.formBuilder.group({
            id: group.id,
            name: [ group.name, Validators.required ],
        });
    }
}
