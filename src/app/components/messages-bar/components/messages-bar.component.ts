import { untilDestroyed } from "ngx-take-until-destroy";
import { Component, OnDestroy } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { finalize } from "rxjs/operators";

import { MessagesHttpService } from "../../../shared/http-services/messages/messages-http.service";
import { Message } from "../../../shared/http-services/messages/models/message.model";
import { MessagesStateService } from "../../../shared/state/messages-state.service";
import { MessageEditorApi } from "./message-editor/models/message-editor-api.model";
import { MessageGroupsEditorApi } from "./groups-editor/models/message-groups-editor-api.model";

@Component({
    selector: "clmr-messages-bar",
    templateUrl: "./messages-bar.component.html",
    styleUrls: ["./messages-bar.component.scss"]
})
export class MessagesBarComponent implements OnDestroy {

    public loading: boolean;
    public messages: Message[];
    public selectedMessagesIds: number[];

    private groupsEditorApi: MessageGroupsEditorApi;
    private editorApi: MessageEditorApi;

    constructor(
        private messagesHttpService: MessagesHttpService,
        private messagesStateService: MessagesStateService,
        private toastrService: ToastrService,
    ) {
        this.loadData();
        this.subscribeOnSelectedMessages();
    }

    public ngOnDestroy() {}

    public onAddNew()
    : void {
        this.editorApi.openModal();
    }

    public onClick(id: number)
    : void {
        this.selectedMessagesIds.includes(id)
            ? this.messagesStateService.unselectMessage(id)
            : this.messagesStateService.selectMessage(id);
    }

    public onDelete(message: Message)
    : void {
        const confirmMessage = `Are you sure you want to delete message "${message.group} | ${message.text.substr(0, 30)}..."?`

        if (!confirm(confirmMessage)) {
            return;
        }

        this.messagesHttpService.delete(message.id)
            .pipe(finalize(() => this.loadData()))
            .subscribe(
                () => this.toastrService.success("Message was successfully deleted."),
                err => this.toastrService.error(err.message),
            );
    }

    public onEdit(message: Message)
    : void {
        this.editorApi.openModal(message);
    }

    public onEditGroups()
    : void {
        this.groupsEditorApi.openModal();
    }

    public onEditorReady(api: MessageEditorApi)
    : void {
        this.editorApi = api;
    }

    public onGroupsEditorReady(api: MessageGroupsEditorApi)
    : void {
        this.groupsEditorApi = api;
    }

    public onReload()
    : void {
        this.loadData();
    }

    public onSelectAll()
    : void {
        this.messagesStateService.selectExactMessages(this.messages.map(m => m.id));
    }

    public onUnselectAll()
    : void {
        this.messagesStateService.unselectAll();
    }

    private loadData()
    : void {
        this.loading = true;

        this.messagesHttpService.getAll()
            .pipe(
                finalize(() => this.loading = false)
            )
            .subscribe(
                res => this.messages = res,
                err => this.toastrService.error(err.message),
            );
    }

    private subscribeOnSelectedMessages()
    : void {
        this.messagesStateService.selectedMessages$
            .pipe(untilDestroyed(this))
            .subscribe(ids => this.selectedMessagesIds = ids);
    }
}
