export class MessageGroupsEditorApi {

    public openModal: () => void;

    constructor(init?: Partial<MessageGroupsEditorApi>) {
        Object.assign(this, init);
    }
}
