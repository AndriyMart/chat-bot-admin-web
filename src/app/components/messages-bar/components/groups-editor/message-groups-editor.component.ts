import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { NgxSmartModalComponent } from "ngx-smart-modal";
import { ToastrService } from "ngx-toastr";
import { FormGroup } from "@angular/forms";
import { finalize } from "rxjs/operators";

import { MessageEditorApi } from "../message-editor/models/message-editor-api.model";
import { MessagesHttpService } from "../../../../shared/http-services/messages/messages-http.service";
import { NamedEntity } from "../../../../shared/models/named-entity.model";
import { MessageGroupEditorApi } from "../group-editor/models/message-group-editor-api.model";

@Component({
    selector: "clmr-message-groups-editor",
    templateUrl: "./message-groups-editor.component.html",
    styleUrls: [ "./message-groups-editor.component.scss" ]
})
export class MessageGroupsEditorComponent implements OnInit {

    @ViewChild("modal")
    public modalComponent: NgxSmartModalComponent;

    @Output()
    public modalClose: EventEmitter<void>;
    @Output()
    public modalReady: EventEmitter<MessageEditorApi>;

    public groups: NamedEntity[];
    public form: FormGroup;
    public loading: boolean;

    private editorApi: MessageGroupEditorApi;

    constructor(
        private messagesHttpService: MessagesHttpService,
        private toastrService: ToastrService,
    ) {
        this.modalClose = new EventEmitter<void>();
        this.modalReady = new EventEmitter<MessageEditorApi>();

        this.loadData();
    }

    public ngOnInit()
    : void {
        this.modalReady.emit({
            openModal: this.openModal.bind(this),
        });
    }

    public onAddNew()
    : void {
        this.editorApi.openModal();
    }

    public onDelete(group: NamedEntity)
    : void {
        const confirmMessage = `Are you sure you want to delete Group ${group.name}?`;

        if (!confirm(confirmMessage)) {
            return;
        }

        this.messagesHttpService.deleteGroup(group.id)
            .pipe(finalize(() => this.loadData()))
            .subscribe(
                () => this.toastrService.success(`Group ${group.name} successfully deleted.`),
                err => this.toastrService.error(err.message),
            );
    }

    public onReload()
    : void {
        this.loadData();
    }

    public onClose()
    : void {
        this.modalClose.emit();
    }

    public onEdit(group: NamedEntity)
    : void {
        this.editorApi.openModal(group);
    }

    public onEditorReady(api: MessageGroupEditorApi)
    : void {
        this.editorApi = api;
    }

    private openModal()
    : void {
        this.modalComponent.open();
    }

    private loadData()
    : void {
        this.loading = true;

        this.messagesHttpService
            .getAllGroups()
            .pipe(finalize(() => this.loading = false))
            .subscribe(
                res => this.groups = res,
                err => this.toastrService.error(err.message),
            );
    }
}

