import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgxSmartModalComponent } from "ngx-smart-modal";

import { Message } from "../../../../shared/http-services/messages/models/message.model";
import { MessageEditorApi } from "./models/message-editor-api.model";
import { AbstractFormBasedComponent } from "../../../../core/components/abstract-form-based.component";
import { MessagesHttpService } from "../../../../shared/http-services/messages/messages-http.service";
import { NamedEntity } from "../../../../shared/models/named-entity.model";
import { ToastrService } from "ngx-toastr";
import { MessageGroupsEditorApi } from "../groups-editor/models/message-groups-editor-api.model";

@Component({
    selector: "clmr-message-editor",
    templateUrl: "./message-editor.component.html",
    styleUrls: [ "./message-editor.component.scss" ]
})
export class MessageEditorComponent extends AbstractFormBasedComponent implements OnInit {

    @ViewChild("modal")
    public modalComponent: NgxSmartModalComponent;

    @Output()
    public modalReady: EventEmitter<MessageEditorApi>;
    @Output()
    public updated: EventEmitter<void>;

    public groupsEditorApi: MessageGroupsEditorApi;
    public groups: NamedEntity[];
    public editMode: boolean;
    public form: FormGroup;

    constructor(
       private formBuilder: FormBuilder,
       private messagesHttpService: MessagesHttpService,
       private toastrService: ToastrService,
    ) {
        super();

        this.modalReady = new EventEmitter<MessageEditorApi>();
        this.updated = new EventEmitter<void>();

        this.loadGroupsOptions();
    }

    public ngOnInit()
    : void {
        this.modalReady.emit({
            openModal: this.openModal.bind(this),
        });
    }

    public onEditGroups()
    : void {
        this.groupsEditorApi.openModal();
    }

    public onSave()
    : void {
        this.markAllFormControlsTouched(this.form);

        if (!this.form.valid) {
            return ;
        }

        const message = new Message({
            id: this.form.value.id,
            text: this.form.value.text,
            group: this.form.value.group
        });

        const source = this.editMode ? this.messagesHttpService.updateMessage : this.messagesHttpService.createMessage;

        source.bind(this.messagesHttpService)(message)
            .subscribe(
                () => {
                    this.toastrService.success(`Message ${message.id} successfully saved.`);
                    this.updated.emit();
                    this.modalComponent.close();
                },
                err => this.toastrService.error(err.message)
            );
    }

    public onGroupsEditorClosed()
    : void {
        this.loadGroupsOptions();
    }

    public onGroupsEditorReady(api: MessageGroupsEditorApi)
    : void {
        this.groupsEditorApi = api;
    }

    private openModal(message?: Message)
    : void {
        this.editMode = !!message;

        this.initForm(message);
        this.modalComponent.open();
    }

    private initForm(message = new Message())
    : void {
        this.form = this.formBuilder.group({
            id: message.id,
            text: [message.text, Validators.required],
            group: message.group_id,
        });
    }

    private loadGroupsOptions()
    : void {
        this.messagesHttpService.getAllGroups()
            .subscribe(
                res => this.groups = res,
                err => this.toastrService.error(err.message),
            );
    }
}
