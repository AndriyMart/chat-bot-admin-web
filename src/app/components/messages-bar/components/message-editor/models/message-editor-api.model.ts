import { Message } from "../../../../../shared/http-services/messages/models/message.model";

export class MessageEditorApi {

    public openModal: (message?: Message) => void;

    constructor(init?: Partial<MessageEditorApi>) {
        Object.assign(this, init);
    }
}
