import { Component, EventEmitter, Input, Output } from "@angular/core";

import { Message } from "../../../../shared/http-services/messages/models/message.model";

@Component({
    selector: "clmr-message",
    templateUrl: "./message.component.html",
    styleUrls: ["./message.component.scss"]
})
export class MessageComponent {

    @Input()
    public message: Message;
    @Input()
    public selected: boolean;

    @Output()
    public delete: EventEmitter<void>;
    @Output()
    public edit: EventEmitter<void>;
    @Output()
    public select: EventEmitter<void>;

    constructor() {
        this.delete = new EventEmitter<void>();
        this.edit = new EventEmitter<void>();
        this.select = new EventEmitter<void>();
    }

    public onEdit(event: Event)
    : void {
        event.stopPropagation();

        this.edit.emit();
    }

    public onSelect()
    : void {
        this.select.emit();
    }

    public onDelete(event: Event)
    : void {
        event.stopPropagation();

        this.delete.emit();
    }
}
