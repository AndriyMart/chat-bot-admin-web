import { NgModule } from "@angular/core";

import { MessagesBarComponent } from "./components/messages-bar.component";
import { MessageComponent } from "./components/message/message.component";
import { SharedModule } from "../../shared/shared.module";
import { MessageEditorComponent } from "./components/message-editor/message-editor.component";
import { MessageGroupsEditorComponent } from "./components/groups-editor/message-groups-editor.component";
import { MessageGroupEditorComponent } from "./components/group-editor/message-group-editor.component";

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        MessageComponent,
        MessagesBarComponent,
        MessageEditorComponent,
        MessageGroupsEditorComponent,
        MessageGroupEditorComponent,
    ],
    exports: [
        MessagesBarComponent,
    ],
})
export class MessagesBarModule { }
