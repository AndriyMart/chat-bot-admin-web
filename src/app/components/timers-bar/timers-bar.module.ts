import { NgModule } from "@angular/core";

import { SharedModule } from "../../shared/shared.module";
import { TimersBarComponent } from "./components/timers-bar.component";
import { TimerComponent } from "./components/timer/timer.component";
import { TimerEditorComponent } from "./components/timer-editor/timer-editor.component";

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        TimerComponent,
        TimersBarComponent,
        TimerEditorComponent,
    ],
    exports: [
        TimersBarComponent,
    ],
})
export class TimersBarModule { }
