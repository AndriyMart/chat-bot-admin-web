import { untilDestroyed } from "ngx-take-until-destroy";
import { Component, OnDestroy } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { finalize } from "rxjs/operators";

import { TimersHttpService } from "../../../shared/http-services/timers/timers-http.service";
import { Timer } from "../../../shared/http-services/timers/models/timer.model";
import { MessagesStateService } from "../../../shared/state/messages-state.service";
import { ChannelsStateService } from "../../../shared/state/channels-state.service";
import { EditorModalApi } from "../../../shared/models/default-modal-api.model";
import { Channel } from "../../../shared/http-services/channels/models/channel.model";
import { ChannelsHttpService } from "../../../shared/http-services/channels/channels-http.service";

@Component({
    selector: "clmr-timers-bar",
    templateUrl: "./timers-bar.component.html",
    styleUrls: ["./timers-bar.component.scss"]
})
export class TimersBarComponent implements OnDestroy {

    public loading: boolean;
    public timers: Timer[];
    public channels: Channel[];

    public selectedMessages: number[];
    public selectedChannel: number;

    private editorApi: EditorModalApi<void>;

    constructor(
        private channelsStateService: ChannelsStateService,
        private messagesStateService: MessagesStateService,
        private channelsHttpService: ChannelsHttpService,
        private timersHttpService: TimersHttpService,
        private toastrService: ToastrService,
    ) {
        this.loadData();

        this.subscribeToSelectedChannelsAndMessages();
    }

    public ngOnDestroy() {}

    public get timerCanBeAdded()
    : boolean {
        return this.selectedMessages && this.selectedMessages.length > 0 && !!this.selectedChannel;
    }

    public onAddNew()
    : void {
        this.editorApi.openModal();
    }

    public onEditorReady(api: EditorModalApi<void>)
    : void {
        this.editorApi = api;
    }

    public onDelete(timer: Timer)
    : void {
        const confirmMessage = `Are you sure you want to delete timer ${timer.channel_id} | once per ${timer.frequency} min?`

        if (!confirm(confirmMessage)) {
            return;
        }

        this.timersHttpService.delete(timer.id)
            .pipe(finalize(() => this.loadData()))
            .subscribe(
                () => this.toastrService.success("Timer was successfully deleted."),
                err => this.toastrService.error(err.message),
            );
    }

    public onReload()
    : void {
        this.loadData();
    }

    private loadData()
    : void {
        this.loading = true;

        this.timersHttpService.getAll()
            .pipe(
                finalize(() => this.loading = false)
            )
            .subscribe(
                res => this.timers = res,
                err => this.toastrService.error(err.message),
            );

        this.channelsHttpService.getAll()
            .subscribe(res => this.channels = res);
    }

    private subscribeToSelectedChannelsAndMessages()
    : void {
        this.messagesStateService.selectedMessages$
            .pipe(untilDestroyed(this))
            .subscribe(ids => this.selectedMessages = ids);

        this.channelsStateService.selectedChannel$
            .pipe(untilDestroyed(this))
            .subscribe(channel => this.selectedChannel = channel);
    }
}
