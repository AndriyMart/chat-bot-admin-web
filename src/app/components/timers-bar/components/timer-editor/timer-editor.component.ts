import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { AbstractFormBasedComponent } from "../../../../core/components/abstract-form-based.component";
import { NgxSmartModalComponent } from "ngx-smart-modal";
import { EditorModalApi } from "../../../../shared/models/default-modal-api.model";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ChannelsHttpService } from "../../../../shared/http-services/channels/channels-http.service";
import { ToastrService } from "ngx-toastr";
import { TimersHttpService } from "../../../../shared/http-services/timers/timers-http.service";

@Component({
    selector: "clmr-timer-editor",
    templateUrl: "./timer-editor.component.html",
    styleUrls: [ "./timer-editor.component.scss" ]
})
export class TimerEditorComponent extends AbstractFormBasedComponent implements OnInit {

    @ViewChild("modal")
    public modalComponent: NgxSmartModalComponent;

    @Input()
    public selectedMessages: number[];
    @Input()
    public selectedChannel: number;

    @Output()
    public saved: EventEmitter<void>;
    @Output()
    public modalReady: EventEmitter<EditorModalApi<void>>;

    public form: FormGroup;

    constructor(
        private timersHttpService: TimersHttpService,
        private formBuilder: FormBuilder,
        private toastrService: ToastrService,
    ) {
        super();

        this.saved = new EventEmitter<void>();
        this.modalReady = new EventEmitter<EditorModalApi<void>>();
    }

    public ngOnInit()
    : void {
        this.modalReady.emit({
            openModal: this.openModal.bind(this),
        });
    }

    public onSave()
    : void {
        this.markAllFormControlsTouched(this.form);

        if (!this.form.valid) {
            return ;
        }

        this.timersHttpService.create({
            frequency: this.form.value.frequency,
            messages: this.selectedMessages,
            channel_id: this.selectedChannel,
        })
            .subscribe(
                res => {
                    this.toastrService.success("Timer was successfully saved");
                    this.saved.emit();
                    this.modalComponent.close();
                },
                err => {
                    this.toastrService.success("Timer was successfully saved");
                    this.saved.emit();
                    this.modalComponent.close();
                },
            )
    }

    private openModal()
    : void {
        this.initForm();
        this.modalComponent.open();
    }

    private initForm()
    : void {
        this.form = this.formBuilder.group({
            frequency: [null, Validators.required],
        })
    }
}
