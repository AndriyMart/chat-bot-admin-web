import { Component, EventEmitter, Input, Output } from "@angular/core";

import { Timer } from "../../../../shared/http-services/timers/models/timer.model";
import { NamedEntity } from "../../../../shared/models/named-entity.model";
import { Channel } from "../../../../shared/http-services/channels/models/channel.model";

@Component({
    selector: "clmr-timer",
    templateUrl: "./timer.component.html",
    styleUrls: ["./timer.component.scss"]
})
export class TimerComponent {

    @Input()
    public channels: Channel[];
    @Input()
    public timer: Timer;

    @Output()
    public delete: EventEmitter<void>;

    constructor() {
        this.delete = new EventEmitter<void>();
    }

    public getChannelName(id: number)
    : string {
        if (!this.channels) {
            return "";
        }

        return this.channels.find(ch => ch.id === id).name;
    }

    public get messages()
    : string {
        return (<NamedEntity[]>this.timer.messages).map(m => m.id).join(", ");
    }

    public onDelete(event: Event)
    : void {
        event.stopPropagation();

        this.delete.emit();
    }
}
