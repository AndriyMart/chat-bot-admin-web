import { Component } from '@angular/core';

@Component({
  selector: 'clmr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'chat-bot-admin-web';
}
